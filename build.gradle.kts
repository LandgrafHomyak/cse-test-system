
plugins {
    kotlin("jvm") version "1.7.0"
}

repositories {
    mavenCentral()
}

kotlin {
    target {
        compilations.all {
            kotlinOptions.jvmTarget = "1.8"
        }
    }
}

tasks.withType<JavaCompile> {
    options.encoding = "UTF-8"
}