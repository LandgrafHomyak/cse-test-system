package ru.ifmo.se.gitlab.cse.nasm_test;

import java.io.InputStream;
import java.util.List;

/**
 * Result of executing test.
 */
interface TestResult {
    /**
     * {@link Task Task}, result of executing which, provides this object.
     */
    Task testInstance();

    /**
     * Current {@link TestSetSummary state} of test.
     */
    TestState state();

    /**
     * Content of test output stream (1).
     */
    InputStream exportStdout();

    /**
     * Content of test errors stream (2).
     */
    InputStream exportStderr();

    /**
     * Caught errors while executing this test.
     */
    List<Throwable> errors();

    /**
     * Optional message describes test result.
     */
    String message();
}

