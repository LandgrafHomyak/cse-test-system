package ru.ifmo.se.gitlab.cse.nasm_test;

/**
 * Future-like object to receive result of executing tests.
 */
public interface TestSetSummary {
    /**
     * Waits until tests are finished.
     */
    void await() throws InterruptedException;

    /**
     * Indicates if test execution is finished.
     */
    boolean isFinished();


    /**
     * Test results.
     *
     * @throws IllegalStateException If tests are not finished.
     */
    Iterable<TestResult> getResults();
}
