package ru.ifmo.se.gitlab.cse.nasm_test;

import java.util.concurrent.Executor;
import java.util.function.Consumer;

/**
 * Interface for building test sets.
 */
public interface TestSetBuilder {
    /**
     * Adds test to set.
     *
     * @param test Test to be added.
     * @throws TestDuplicationException If task instance already added.
     */
    void addTask(Task test);

    /**
     * Sets dependency between two tests or tasks that are already added to set.
     * {@code slave} task will be executed only if {@code master} task finished successfully.
     *
     * @param slave  Dependent task.
     * @param master Dependency.
     * @throws DependencyCycleException If new dependency makes a cycle.
     * @throws TaskNotFoundException    If tasks not added to set.
     */
    void addDependency(Task slave, Task master);

    /**
     * Launches tests on specified executor and sends its results to the observer
     * in thread in which corresponding test was executed.
     *
     * @return {@link TestSetSummary Results of all tests}.
     */
    TestSetSummary launchTests(Executor executor, Consumer<TestResult> observer);

    /**
     * Launches tests on specified executor.
     *
     * @return {@link TestSetSummary Results of all tests}.
     */
    TestSetSummary launchTests(Executor executor);
}
