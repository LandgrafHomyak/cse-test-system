package ru.ifmo.se.gitlab.cse.nasm_test;

public abstract class Task {
    public final String name;
    public final boolean isService;

    public Task(final String name, final boolean isService) {
        this.name = name;
        this.isService = isService;
    }


    abstract public void run(TestingEnvironment environment);
}
