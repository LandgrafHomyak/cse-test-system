package ru.ifmo.se.gitlab.cse.nasm_test;

public final class TaskNotFoundException extends RuntimeException {
    public final Task task;
    public final TestSetBuilderImpl set;

    public TaskNotFoundException(final Task task, final TestSetBuilderImpl set) {
        super("Task not found in set");
        this.task = task;
        this.set = set;
    }
}
