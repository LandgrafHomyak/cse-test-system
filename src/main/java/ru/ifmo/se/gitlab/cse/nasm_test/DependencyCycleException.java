package ru.ifmo.se.gitlab.cse.nasm_test;

public final class DependencyCycleException extends RuntimeException {
    final Task[] chain;

    public DependencyCycleException(final Task[] chain) {
        super(String.format("Tasks dependency cycle: %s", DependencyCycleException.joinChainNames(chain)));
        this.chain = chain;
    }

    private static String joinChainNames(final Task[] chain) {
        if (chain == null)
            return "";
        if (chain.length <= 0)
            return "";
        final StringBuilder b = new StringBuilder(chain[0].name);
        for (int i = 1; i < chain.length; i++) {
            b.append(" -> ");
            b.append(chain[i].name);
        }
        return b.toString();
    }
}
