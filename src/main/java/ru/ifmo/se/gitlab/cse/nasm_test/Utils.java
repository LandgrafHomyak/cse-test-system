package ru.ifmo.se.gitlab.cse.nasm_test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.String;
import java.io.InputStream;
import java.util.function.Supplier;
import java.lang.Process;
import java.lang.ProcessBuilder;

public final class Utils {
    private Utils() {
    }

    /**
     * Reads whole {@link java.io.InputStream stream} into {@link java.lang.String string}.
     *
     * @param is Stream to be read.
     * @return Stream content.
     * @throws IOException See stream's doc.
     */
    public static String dumpStream(InputStream is) throws IOException {
        final ByteArrayOutputStream accumulator = new ByteArrayOutputStream();
        final byte[] buffer = new byte[1024];
        int length;
        while ((length = is.read(buffer)) != -1) {
            accumulator.write(buffer, 0, length);
        }
        return accumulator.toString("UTF-8");
    }

    /**
     * Loads text file from resources.
     *
     * @param path Path to file in resources namespace.
     * @return File content.
     * @throws IOException See resource loader's doc.
     */
    public static String loadTextResource(String path) throws IOException {
        InputStream stream = Utils.class.getResourceAsStream(path);
        return stream == null ? null : Utils.dumpStream(stream);
    }

    /**
     * {@link Supplier} that can throw {@link IOException}.
     *
     * @param <T> Type of objets produced by supplier.
     */
    public interface IOExceptionSupplier<T> {
        /**
         * Supplies object of type {@code T}.
         *
         * @return Supplied object.
         * @throws IOException if something went wrong.
         */
        T get() throws IOException;
    }

    /**
     * Wraps supplier call and catches {@link IOException} (to stderr) replacing it with null.
     *
     * @param supplier Supplier, that can throw {@link IOException}.
     * @param <T>      Type of object produced by supplier.
     * @return Object returned by supplier if success, {@code null} otherwise.
     */
    public static <T> T catchToNull(IOExceptionSupplier<T> supplier) {
        try {
            return supplier.get();
        } catch (IOException t) {
            t.printStackTrace();
            return null;
        }
    }

    /**
     * Wraps supplier call and catches {@link IOException} (to stderr).
     * If error occurred, exits process with specified {@code code} (e.g. static resource loading failed).
     *
     * @param code     Process exit code if supplier failed.
     * @param supplier Supplier, that can throw {@link IOException}.
     * @param <T>      Type of object produced by supplier.
     * @return Object returned by supplier if success.
     */
    public static <T> T catchToExit(
            @SuppressWarnings("SameParameterValue") int code,
            IOExceptionSupplier<T> supplier
    ) {
        try {
            return supplier.get();
        } catch (IOException t) {
            t.printStackTrace();
            System.exit(code);
            return null;
        }
    }

    /**
     * Copies content of {@code src} stream int {@code dst} stream.
     *
     * @param src Source stream.
     * @param dst Destination stream.
     * @throws IOException See streams' docs.
     */
    public static void copyStream(OutputStream dst, InputStream src) throws IOException {
        final byte[] buf = new byte[1024];
        int length;
        while ((length = src.read(buf)) != -1) {
            dst.write(buf, 0, length);
        }
    }

    public static Process startProcess(String... argv) throws IOException {
        ProcessBuilder pb = new ProcessBuilder(argv);
        return pb.start();
    }

    public static Process executeProcess(String... argv) throws IOException, InterruptedException {
        ProcessBuilder pb = new ProcessBuilder(argv);
        Process p = pb.start();
        p.wait();
        return p;
    }

    public static String multilineString(String... lines) {
        if (lines.length == 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder(lines[0]);
        for (int i = 1; i < lines.length; i++) {
            sb.append('\n');
            sb.append(lines[i]);
        }
        return sb.toString();
    }
}
