package ru.ifmo.se.gitlab.cse.nasm_test;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;
import java.util.function.Consumer;
import java.util.function.Function;

public final class TestSetBuilderImpl implements TestSetBuilder {
    private static class Entry {
        final Task owner;
        final HashMap<String, TestSetBuilderImpl.Entry> slaves = new HashMap<>();

        Entry(final Task owner) {
            this.owner = owner;
        }
    }

    private final HashMap<String, TestSetBuilderImpl.Entry> tasks = new HashMap<>();

    @Override
    public void addTask(Task test) {
        TestSetBuilderImpl.Entry oldTest = this.tasks.put(test.name, new Entry(test));
        if (oldTest != null)
            throw new TestDuplicationException(oldTest.owner, test, this);
    }

    @Override
    public void addDependency(final Task slave, final Task master) {
        if (slave == master)
            throw new DependencyCycleException(new Task[]{slave, master});

        final TestSetBuilderImpl.Entry slaveEntry = this.tasks.get(slave.name);
        final TestSetBuilderImpl.Entry masterEntry = this.tasks.get(master.name);
        if (slaveEntry == null || slaveEntry.owner != slave)
            throw new TaskNotFoundException(slave, this);
        if (masterEntry == null || masterEntry.owner != master)
            throw new TaskNotFoundException(master, this);

        if (this.findSlaveTask(slaveEntry, master))
            throw new DependencyCycleException(null);

        if (!this.findSlaveTask(masterEntry, slave))
            masterEntry.slaves.put(slave.name, slaveEntry);
    }


    private boolean findSlaveTask(final TestSetBuilderImpl.Entry startEntry, final Task expectedTask) {
        final TestSetBuilderImpl.Entry[] queue = new TestSetBuilderImpl.Entry[this.tasks.size()];
        queue[0] = startEntry;
        int qStart = 0, qEnd = 1;
        while (qStart < qEnd) {
            final TestSetBuilderImpl.Entry entry = queue[qStart++];
            if (entry.owner == expectedTask)
                return true;

            for (TestSetBuilderImpl.Entry slave : entry.slaves.values()) {
                queue[qEnd++] = slave;
            }
        }
        return false;
    }

    private static final class TestSetSummaryImpl implements TestSetSummary {
        private final class TestEnvironmentImpl implements TestResult, TestingEnvironment, Runnable {
            private final Task test;
            private final BufferedInputOutputStream stdoutIn = new BufferedInputOutputStream();
            private final InputStream stdoutOut = this.stdoutIn.reader();
            private final BufferedInputOutputStream stderrIn = new BufferedInputOutputStream();
            private final InputStream stderrOut = this.stderrIn.reader();
            private final ArrayList<Throwable> errors = new ArrayList<>();
            private String message = null;
            private TestState state = TestState.PENDING;
            final TestSetBuilderImpl.TestSetSummaryImpl.TestEnvironmentImpl[] slaves;
            private int dependenciesLeft;
            int posInHeap;

            TestEnvironmentImpl(
                    final Task master,
                    final TestSetBuilderImpl.TestSetSummaryImpl.TestEnvironmentImpl[] slaves,
                    final int dependenciesLeft
            ) {
                this.test = master;
                this.slaves = slaves;
                this.dependenciesLeft = dependenciesLeft;
            }

            @Override
            public Task testInstance() {
                return this.test;
            }

            @Override
            public TestState state() {
                return this.state;
            }

            @Override
            public OutputStream stdout() {
                return this.stdoutIn;
            }

            @Override
            public OutputStream stderr() {
                return this.stderrIn;
            }

            @Override
            public void addException(Throwable error) {
                this.errors.add(error);
            }

            @Override
            public void setTestFailed(String message) {
                if (this.state.errorMessageOnResultSetting != null)
                    throw new IllegalStateException(this.state.errorMessageOnResultSetting);
                this.message = message;
                this.state = TestState.FAILED;
            }

            @Override
            public void setTestSuccess(String message) {
                if (this.state.errorMessageOnResultSetting != null)
                    throw new IllegalStateException(this.state.errorMessageOnResultSetting);
                this.message = message;
                this.state = TestState.SUCCESSFUL;
            }

            @Override
            public void setTestSuccess() {
                if (this.state.errorMessageOnResultSetting != null)
                    throw new IllegalStateException(this.state.errorMessageOnResultSetting);
                this.message = null;
                this.state = TestState.SUCCESSFUL;
            }

            @Override
            public String message() {
                if (this.state.errorMessageOnResultEarlyAccess != null)
                    throw new IllegalStateException(this.state.errorMessageOnResultEarlyAccess);
                return this.message;
            }

            @Override
            public InputStream exportStdout() {
                if (this.state.errorMessageOnResultEarlyAccess != null)
                    throw new IllegalStateException(this.state.errorMessageOnResultEarlyAccess);
                return this.stdoutOut;
            }

            @Override
            public InputStream exportStderr() {
                if (this.state.errorMessageOnResultEarlyAccess != null)
                    throw new IllegalStateException(this.state.errorMessageOnResultEarlyAccess);
                return this.stderrOut;
            }

            @Override
            public List<Throwable> errors() {
                if (this.state.errorMessageOnResultEarlyAccess != null)
                    throw new IllegalStateException(this.state.errorMessageOnResultEarlyAccess);
                return this.errors;
            }

            @Override
            public void run() {
                try {
                    if (this.state != TestState.SKIPPED) {
                        this.state = TestState.EXECUTING;
                        try {
                            this.test.run(this);
                        } catch (Throwable e) {
                            this.errors.add(e);
                            if (this.state == TestState.EXECUTING) {
                                this.state = TestState.FAILED;
                                this.message = "Uncaught error while executing test";
                            }
                        }
                        switch (this.state) {
                            case EXECUTING:
                                this.state = TestState.FAILED;
                                this.message = "Test result not set";
                            case FAILED:
                                break;
                            case SUCCESSFUL:
                                for (TestEnvironmentImpl slave : this.slaves) {
                                    slave.dependenciesLeft--;
                                    slave.heapify();
                                }
                                return;
                        }
                    }
                    for (TestEnvironmentImpl slave : this.slaves) {
                        slave.state = TestState.SKIPPED;
                        slave.dependenciesLeft--;
                        slave.heapify();
                    }
                } finally {
                    TestSetBuilderImpl.TestSetSummaryImpl.this.sync.countDown();
                    TestSetBuilderImpl.TestSetSummaryImpl.this.results.add(this);
                    TestSetBuilderImpl.TestSetSummaryImpl.this.runNext();
                    TestSetBuilderImpl.TestSetSummaryImpl.this.observer.accept(this);
                }
            }

            void heapify() {
                while (this.posInHeap > 0 && this.dependenciesLeft < TestSetBuilderImpl.TestSetSummaryImpl.this.heap[(this.posInHeap + 1) / 2 - 1].dependenciesLeft) {
                    TestSetBuilderImpl.TestSetSummaryImpl.this.heap[this.posInHeap] = TestSetBuilderImpl.TestSetSummaryImpl.this.heap[(this.posInHeap + 1) / 2 - 1];
                    TestSetBuilderImpl.TestSetSummaryImpl.this.heap[this.posInHeap].posInHeap = this.posInHeap;
                    this.posInHeap = (this.posInHeap + 1) / 2 - 1;
                    TestSetBuilderImpl.TestSetSummaryImpl.this.heap[this.posInHeap] = this;
                }
            }
        }

        private static class DependenciesCountNotSet implements Function<String, Integer> {
            static DependenciesCountNotSet INSTANCE = new DependenciesCountNotSet();

            private DependenciesCountNotSet() {
            }

            @Override
            public Integer apply(String key) {
                return 0;
            }
        }

        private final TestSetBuilderImpl.TestSetSummaryImpl.TestEnvironmentImpl[] heap;
        private final Executor executor;
        private final Consumer<TestResult> observer;
        private final CountDownLatch sync;
        private final ArrayList<TestResult> results = new ArrayList<>();

        TestSetSummaryImpl(
                final Executor executor,
                final Iterable<TestSetBuilderImpl.Entry> tasks,
                final Consumer<TestResult> observer
        ) {
            this.executor = executor;
            this.observer = observer;

            final HashMap<String, Integer> dependenciesCount = new HashMap<>();
            for (TestSetBuilderImpl.Entry entry : tasks) {
                for (String key : entry.slaves.keySet()) {
                    dependenciesCount.put(
                            key,
                            dependenciesCount.computeIfAbsent(
                                    key,
                                    TestSetBuilderImpl.TestSetSummaryImpl.DependenciesCountNotSet.INSTANCE
                            ) + 1
                    );
                }
            }

            final HashMap<String, TestSetBuilderImpl.TestSetSummaryImpl.TestEnvironmentImpl> envInstances = new HashMap<>();

            for (TestSetBuilderImpl.Entry entry : tasks) {
                envInstances.put(
                        entry.owner.name,
                        new TestSetBuilderImpl.TestSetSummaryImpl.TestEnvironmentImpl(
                                entry.owner,
                                new TestSetBuilderImpl.TestSetSummaryImpl.TestEnvironmentImpl[entry.slaves.size()],
                                dependenciesCount.computeIfAbsent(
                                        entry.owner.name,
                                        TestSetBuilderImpl.TestSetSummaryImpl.DependenciesCountNotSet.INSTANCE
                                )
                        )
                );
            }

            for (TestSetBuilderImpl.Entry entry : tasks) {
                final TestSetBuilderImpl.TestSetSummaryImpl.TestEnvironmentImpl env = envInstances.get(entry.owner.name);
                int i = 0;
                for (String name : entry.slaves.keySet()) {
                    env.slaves[i] = envInstances.get(name);
                }
            }

            this.heap = new TestSetBuilderImpl.TestSetSummaryImpl.TestEnvironmentImpl[dependenciesCount.size()];
            int heapSize = 0;
            for (TestSetBuilderImpl.TestSetSummaryImpl.TestEnvironmentImpl env : envInstances.values()) {
                env.posInHeap = heapSize;
                this.heap[heapSize++] = env;
                env.heapify();
            }

            this.sync = new CountDownLatch(envInstances.size());
            this.runNext();
        }

        void runNext() {
            while (true) {
                if (this.heap[0] == null)
                    return;
                if (this.heap[0].dependenciesLeft > 0)
                    return;

                final TestSetBuilderImpl.TestSetSummaryImpl.TestEnvironmentImpl nextTask = this.heap[0];

                int i = 0;
                TestSetBuilderImpl.TestSetSummaryImpl.TestEnvironmentImpl l, r;
                while (true) {
                    l = (i << 1) + 1 < this.heap.length ? this.heap[(i << 1) + 1] : null;
                    r = (i + 1) << 1 < this.heap.length ? this.heap[(i + 1) << 1] : null;
                    if (r != null) {
                        if (l != null) {
                            if (r.dependenciesLeft < l.dependenciesLeft) {
                                this.heap[i] = r;
                                r.posInHeap = i;
                                i = (i + 1) << 1;
                            } else {
                                this.heap[i] = l;
                                l.posInHeap = i;
                                i = (i << 1) + 1;
                            }
                        } else {
                            this.heap[i] = r;
                            r.posInHeap = i;
                            i = (i + 1) << 1;
                        }
                    } else {
                        if (l != null) {
                            this.heap[i] = l;
                            l.posInHeap = i;
                            i = (i << 1) + 1;
                        } else {
                            this.heap[i] = null;
                            break;
                        }
                    }
                }
                this.executor.execute(nextTask);
            }
        }

        @Override
        public void await() throws InterruptedException {
            this.sync.await();
        }

        @Override
        public boolean isFinished() {
            return this.heap[0] == null;
        }

        @Override
        public Iterable<TestResult> getResults() {
            if (this.heap[0] != null)
                throw new IllegalStateException("Accessing test results before finishing execution");
            return this.results;
        }
    }

    @Override
    public TestSetSummary launchTests(Executor executor, Consumer<TestResult> observer) {
        return new TestSetSummaryImpl(executor, this.tasks.values(), observer);
    }

    private static class NopConsumer implements Consumer<TestResult> {
        static TestSetBuilderImpl.NopConsumer INSTANCE = new TestSetBuilderImpl.NopConsumer();

        @Override
        public void accept(TestResult testResult) {
        }
    }

    @Override
    public TestSetSummary launchTests(Executor executor) {
        return new TestSetSummaryImpl(executor, this.tasks.values(), TestSetBuilderImpl.NopConsumer.INSTANCE);
    }
}
