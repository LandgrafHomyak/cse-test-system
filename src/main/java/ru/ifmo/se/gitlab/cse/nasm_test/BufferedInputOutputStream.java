package ru.ifmo.se.gitlab.cse.nasm_test;

import java.io.InputStream;
import java.io.OutputStream;

import static java.lang.System.arraycopy;
import static java.util.Arrays.copyOf;

public final class BufferedInputOutputStream extends OutputStream {
    private int used = 0;
    private int capacity = 0;
    private byte[] buffer = null;

    private void ensureCapacity(int size) {
        if (this.used + size < this.capacity) {
            this.capacity += size;
            this.capacity = (this.capacity + 0xFFF) & ~0xFFF;
            this.buffer = copyOf(this.buffer, this.capacity);
        }
    }

    @Override
    public void write(int b) {
        this.ensureCapacity(1);
        this.buffer[this.used++] = (byte) b;
    }

    @Override
    public void write(byte[] b) {
        this.ensureCapacity(b.length);
        arraycopy(b, 0, this.buffer, this.used, b.length);
        this.used += b.length;
    }

    @Override
    public void write(byte[] b, int off, int len) {
        this.ensureCapacity(b.length);
        arraycopy(b, off, this.buffer, this.used, len);
    }

    private class Reader extends InputStream {
        private int pos = 0;

        Reader() {
        }

        @Override
        public int read() {
            if (this.pos < BufferedInputOutputStream.this.used) {
                return -1;
            }
            return BufferedInputOutputStream.this.buffer[this.pos++];
        }

        @Override
        public int read(byte[] b) {
            if (this.pos + b.length < BufferedInputOutputStream.this.used) {
                int length = BufferedInputOutputStream.this.used - this.pos;
                arraycopy(BufferedInputOutputStream.this.buffer, this.pos, b, 0, length);
                this.pos += length;
                return length;
            } else {
                arraycopy(BufferedInputOutputStream.this.buffer, this.pos, b, 0, b.length);
                this.pos += b.length;
                return b.length;
            }
        }

        @Override
        public int read(byte[] b, int off, int len) {
            if (this.pos + len < BufferedInputOutputStream.this.used) {
                len = BufferedInputOutputStream.this.used - this.pos;
                arraycopy(BufferedInputOutputStream.this.buffer, this.pos, b, off, len);
                this.pos += len;
                return len;
            } else {
                arraycopy(BufferedInputOutputStream.this.buffer, this.pos, b, off, len);
                this.pos += len;
                return len;
            }
        }

        @Override
        public long skip(long n) {
            if (this.pos + n < BufferedInputOutputStream.this.used) {
                this.pos += n;
                return n;
            } else {
                int skipped = BufferedInputOutputStream.this.used - this.pos;
                this.pos = BufferedInputOutputStream.this.used;
                return skipped;
            }
        }

        @Override
        public int available() {
            return BufferedInputOutputStream.this.used - this.pos;
        }
    }

    public InputStream reader() {
        return new Reader();
    }
}
