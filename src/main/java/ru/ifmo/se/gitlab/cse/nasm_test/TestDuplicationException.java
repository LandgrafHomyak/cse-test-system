package ru.ifmo.se.gitlab.cse.nasm_test;

/**
 * Throws when {@link Task task} added to {@link TestSetBuilder set} already in it.
 */
public final class TestDuplicationException extends RuntimeException {
    /**
     * Test that was added first.
     */
    public final Task old;

    /**
     * Test that was added before exception thrown.
     */
    public final Task _new;
    /**
     * {@link TestSetBuilder Set}, that contains two tests with same name.
     */
    public final TestSetBuilder set;

    public TestDuplicationException(
            final Task old,
            final Task _new,
            final TestSetBuilder set
    ) {
        super("Two tests have same name in test set");
        this.old = old;
        this._new = _new;
        this.set = set;
    }
}
