package ru.ifmo.se.gitlab.cse.nasm_test;

import java.io.OutputStream;

/**
 * Environment for test execution.
 */
public interface TestingEnvironment {
    /**
     * Saves an exception caught while executing test.
     *
     * @param error Caught exception instance.
     */
    void addException(Throwable error);

    /**
     * Marks test as failed.
     *
     * @param message Description why test failed.
     */
    void setTestFailed(String message);

    /**
     * Marks test as successful.
     *
     * @param message Description why test successful.
     */
    void setTestSuccess(String message);

    /**
     * Marks test as successful.
     */
    void setTestSuccess();

    /**
     * Stream in which may be redirected {@code stdout} of tested program.
     */
    OutputStream stdout();

    /**
     * Stream in which may be redirected {@code stderr} of tested program.
     */
    OutputStream stderr();
}
