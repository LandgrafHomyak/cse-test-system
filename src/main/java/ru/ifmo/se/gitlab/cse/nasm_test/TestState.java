package ru.ifmo.se.gitlab.cse.nasm_test;

public enum TestState {
    PENDING(
            "Changing test result before test is executing",
            "Access to test outputs before execution"
    ),
    EXECUTING(
            null,
            "Access to test outputs while execution"
    ),
    SUCCESSFUL(
            "Changing test result after finishing",
            null
    ),
    FAILED(
            "Changing test result after finishing",
            null
    ),
    SKIPPED(
            "Changing result in skipped test",
            "Access to outputs of skipped test"
    );

    public final String errorMessageOnResultSetting;
    public final String errorMessageOnResultEarlyAccess;

    TestState(
            final String errorMessageOnResultSetting,
            final String errorMessageOnResultEarlyAccess
    ) {
        this.errorMessageOnResultSetting = errorMessageOnResultSetting;
        this.errorMessageOnResultEarlyAccess = errorMessageOnResultEarlyAccess;
    }
}
