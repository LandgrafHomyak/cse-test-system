package ru.ifmo.se.gitlab.cse.nasm_test;

import java.io.FileWriter;
import java.io.IOException;
import java.lang.String;
import java.nio.file.Path;
import java.nio.file.Paths;

public abstract class NasmCompileTask extends Task {
    protected static final String beforeCall =
            Utils.catchToExit(1, () -> Utils.loadTextResource("/before_call.asm"));
    protected static final String afterCall =
            Utils.catchToExit(1, () -> Utils.loadTextResource("/after_call.asm"));
    private static final String namePrefix = "[compile] ";
    private static final Object taskNoMonitor = new Object();
    private static int nextTaskNo = 0;

    private final String fileName;
    private final Path workingDirectory;
    @SuppressWarnings("FieldCanBeLocal")
    private final int taskNo;

    public NasmCompileTask(
            final String name,
            final Path workingDirectory,
            final String fileName
    ) {
        super(String.format("%s%s", NasmCompileTask.namePrefix, name), true);
        this.workingDirectory = workingDirectory.toAbsolutePath();
        synchronized (NasmCompileTask.taskNoMonitor) {
            this.taskNo = NasmCompileTask.nextTaskNo++;
        }
        if (fileName == null) {
            this.fileName = String.format("%d", this.taskNo);
        } else {
            this.fileName = fileName;
        }
    }

    public NasmCompileTask(
            final String name,
            final Path workingDirectory
    ) {
        this(name, workingDirectory, null);
    }

    public NasmCompileTask(
            final String name,
            final String fileName
    ) {
        this(name, Paths.get("."), fileName);
    }

    public NasmCompileTask(final String name) {
        this(name, Paths.get("."), null);
    }


    @Override
    public final void run(TestingEnvironment environment) {
        final Path asmPath = this.workingDirectory
                .resolve(String.format("%s.asm", this.fileName));
        final String objPath = this.workingDirectory
                .resolve(String.format("%s.o", this.fileName))
                .toString();
        final String exePath = this.workingDirectory
                .resolve(this.fileName)
                .toString();

        try {
            final FileWriter asmFile = new FileWriter(asmPath.toFile());
            asmFile.write(this.generateSource());
            asmFile.close();
        } catch (IOException e) {
            environment.addException(e);
            environment.setTestFailed("Error while saving test source.");
            return;
        }

        try {
            Process p = Utils.startProcess("nasm", "-f", "elf64", asmPath.toString(), "-o", objPath);
            p.wait();
            Utils.copyStream(environment.stdout(), p.getInputStream());
            Utils.copyStream(environment.stderr(), p.getErrorStream());
            /* Sentinel lines */
            environment.stdout().write('\n');
            environment.stderr().write('\n');
            if (p.exitValue() != 0) {
                environment.setTestFailed(String.format("Compiler failed with code %d.", p.exitValue()));
                return;
            }
        } catch (IOException e) {
            environment.addException(e);
            environment.setTestFailed("IO exception while compiling.");
            return;
        } catch (InterruptedException e) {
            environment.addException(e);
            environment.setTestFailed("Compilation was interrupted.");
            return;
        }

        try {
            Process p = Utils.startProcess("ld", "-o", exePath, objPath);
            p.wait();
            Utils.copyStream(environment.stdout(), p.getInputStream());
            Utils.copyStream(environment.stderr(), p.getErrorStream());
            if (p.exitValue() != 0) {
                environment.setTestFailed(String.format("Linker failed with code %d.", p.exitValue()));
                return;
            }
        } catch (IOException e) {
            environment.addException(e);
            environment.setTestFailed("IO exception while linking.");
            return;
        } catch (InterruptedException e) {
            environment.addException(e);
            environment.setTestFailed("Linkage was interrupted.");
            return;
        }

        environment.setTestSuccess();
    }

    protected abstract String generateSource();
}
