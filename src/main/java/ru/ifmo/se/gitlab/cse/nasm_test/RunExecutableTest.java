package ru.ifmo.se.gitlab.cse.nasm_test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.concurrent.TimeUnit;

import static ru.ifmo.se.gitlab.cse.nasm_test.Utils.copyStream;
import static ru.ifmo.se.gitlab.cse.nasm_test.Utils.dumpStream;
import static ru.ifmo.se.gitlab.cse.nasm_test.Utils.startProcess;

public abstract class RunExecutableTest extends Task {
    private final Path fileToExecute;

    public RunExecutableTest(final String name, final Path fileToExecute, boolean isService) {
        super(name, isService);
        this.fileToExecute = fileToExecute;
    }

    public RunExecutableTest(final String name, final Path fileToExecute) {
        this(name, fileToExecute, false);
    }

    @Override
    public final void run(TestingEnvironment environment) {
        try {
            final String[] argvArgs = this.argv();
            final String[] argv = new String[argvArgs.length + 1];
            argv[0] = this.fileToExecute.toString();
            System.arraycopy(argvArgs, 0, argv, 1, argvArgs.length);
            Process p = startProcess(argv);
            copyStream(p.getOutputStream(), new ByteArrayInputStream(this.input().getBytes(StandardCharsets.UTF_8)));
            p.waitFor(this.timeout(), TimeUnit.MILLISECONDS);
            if (p.isAlive()) {
                p.destroyForcibly();
                environment.setTestFailed("Timed out");
                return;
            }
            assertOutput(environment, p.exitValue(), dumpStream(p.getInputStream()), dumpStream(p.getErrorStream()));
        } catch (IOException e) {
            environment.addException(e);
            environment.setTestFailed("Error with IO while testing.");
        } catch (InterruptedException e) {
            environment.addException(e);
            environment.setTestFailed("Test was interrupted");
        }
    }

    protected String[] argv() {
        return new String[0];
    }

    protected String input() {
        return "";
    }

    protected abstract void assertOutput(TestingEnvironment env, int exitCode, String stdout, String stderr);

    protected long timeout() {
        return 1000;
    }
}
